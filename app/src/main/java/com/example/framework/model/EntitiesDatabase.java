package com.example.framework.model;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Album.class, ToDo.class, Post.class}, version = 1, exportSchema = false)
public abstract class EntitiesDatabase extends RoomDatabase {
    public abstract ToDoDao toDoDao();

    public abstract AlbumDao albumDao();

    public abstract PostDao postDao();
}
