package com.example.framework.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ToDoDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertToDo(ToDo toDo);

    @Query("SELECT * FROM ToDo")
    List<ToDo> getToDos();
}
