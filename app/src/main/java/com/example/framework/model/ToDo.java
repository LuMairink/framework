package com.example.framework.model;

import com.google.gson.annotations.Expose;

@androidx.room.Entity
public class ToDo extends Entity {
    @Expose
    private boolean completed;

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
