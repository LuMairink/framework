package com.example.framework.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PostDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertPost(Post post);

    @Query("SELECT * FROM Post")
    List<Post> getPosts();
}
