package com.example.framework.model;

import com.google.gson.annotations.Expose;

@androidx.room.Entity
public class Post extends Entity {
    @Expose
    private String body;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
