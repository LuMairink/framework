package com.example.framework.model;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface AlbumDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertAlbum(Album album);

    @Query("SELECT * FROM Album")
    List<Entity> getAlbums();
}

