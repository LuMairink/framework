package com.example.framework.ui.albums;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.framework.MainActivity;
import com.example.framework.R;
import com.example.framework.model.Album;
import com.example.framework.request.ListApi;
import com.example.framework.request.RetrofitBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AlbumsFragment extends Fragment {
    private List<Album> albums;
    private AlbumAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_albums, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.albums_list);
        adapter = new AlbumAdapter(getContext(), new ArrayList<Album>());

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(
                new DividerItemDecoration(Objects.requireNonNull(getContext()), DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Retrofit retrofit = RetrofitBuilder.getRetrofit();
        ListApi api = retrofit.create(ListApi.class);
        Call<List<Album>> call = api.getAlbums();
        call.enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(@NonNull Call<List<Album>> call, @NonNull Response<List<Album>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getContext(), "Erro na requisição", Toast.LENGTH_LONG).show();
                    return;
                }

                albums = response.body();
                final MainActivity activity = ((MainActivity) getContext());
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (activity != null) {
                            for (Album album : albums) {
                                activity.db.albumDao().insertAlbum(album);
                            }
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    adapter.setAlbums(albums);
                                }
                            });
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(@NonNull Call<List<Album>> call, @NonNull Throwable t) {
                Toast.makeText(getContext(), "Erro na requisição", Toast.LENGTH_LONG).show();
            }
        });
    }
}
