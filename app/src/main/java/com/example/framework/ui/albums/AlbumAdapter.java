package com.example.framework.ui.albums;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.framework.model.Album;

import java.util.List;


public class AlbumAdapter  extends RecyclerView.Adapter<AlbumAdapter.AlbumHolder> {
        private Context context;
        private List<Album> albums;

        public AlbumAdapter(Context context, List<Album> albums) {
            this.albums = albums;
            this.context = context;
        }

        @NonNull
        @Override
        public AlbumHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return new AlbumHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(android.R.layout.simple_list_item_1, viewGroup, false));
        }

        @Override
        public void onBindViewHolder(@NonNull AlbumHolder viewHolder, int position) {
            Album album = albums.get(position);
            viewHolder.text1.setText(album.getTitle());
        }

        @Override
        public int getItemCount() {
            return albums != null ? albums.size() : 0;
        }

        public void setAlbums(List<Album> albums) {
            this.albums = albums;
            notifyDataSetChanged();
        }

        static class AlbumHolder extends RecyclerView.ViewHolder {

            TextView text1;

            AlbumHolder(View itemView) {
                super(itemView);
                text1 = itemView.findViewById(android.R.id.text1);
            }
        }
    }


