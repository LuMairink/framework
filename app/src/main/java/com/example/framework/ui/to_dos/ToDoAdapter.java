package com.example.framework.ui.to_dos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.framework.R;
import com.example.framework.model.ToDo;

import java.util.List;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.ToDoHolder> {
    private Context context;
    private List<ToDo> toDos;

    public ToDoAdapter(Context context, List<ToDo> toDos) {
        this.toDos = toDos;
        this.context = context;
    }

    @NonNull
    @Override
    public ToDoHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ToDoHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(android.R.layout.simple_list_item_2, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ToDoHolder viewHolder, int position) {
        ToDo toDo = toDos.get(position);
        viewHolder.text1.setText(toDo.getTitle());
        String completed = context.getString(toDo.isCompleted() ? R.string.yes : R.string.no);
        viewHolder.text2.setText(String.format("%s: %s", context.getString(R.string.completed), completed));
    }

    @Override
    public int getItemCount() {
        return toDos != null ? toDos.size() : 0;
    }

    public void setToDos(List<ToDo> toDos) {
        this.toDos = toDos;
        notifyDataSetChanged();
    }

    static class ToDoHolder extends RecyclerView.ViewHolder {

        TextView text1;
        TextView text2;

        ToDoHolder(View itemView) {
            super(itemView);
            text1 = itemView.findViewById(android.R.id.text1);
            text2 = itemView.findViewById(android.R.id.text2);

        }
    }
}

