package com.example.framework.request;

import com.example.framework.model.Album;
import com.example.framework.model.Post;
import com.example.framework.model.ToDo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ListApi {
    @GET("/todos")
    Call<List<ToDo>> getToDos();

    @GET("/albums")
    Call<List<Album>> getAlbums();

    @GET("/posts")
    Call<List<Post>> getPosts();


}
